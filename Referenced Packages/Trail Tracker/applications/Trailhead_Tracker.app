<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>Badge__c</defaultLandingTab>
    <description>The fast, fun, easy way to learn Salesforce.</description>
    <formFactors>Large</formFactors>
    <label>Trail Tracker</label>
    <logo>Trailhead/trailheadLogo_png.png</logo>
    <tab>User_Badge__c</tab>
    <tab>Badge__c</tab>
    <tab>Trailmix__c</tab>
    <tab>Trail__c</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>TrailheadSetup</tab>
    <tab>expense2__c</tab>
    <tab>Tower__c</tab>
    <tab>naghma__c</tab>
    <tab>Battle_Station__c</tab>
</CustomApplication>
