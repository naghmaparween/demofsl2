public class NavigationTest_Controller1 {
    public string AccountId{get;set;}
    public string UITheme {get;set;}
    public  NavigationTest_Controller1(){
        PageReference pageRef = ApexPages.currentPage();      
        AccountId = pageRef.getParameters().get('AccountId'); 
       	UITheme = UserInfo.getUiThemeDisplayed();
     }
}